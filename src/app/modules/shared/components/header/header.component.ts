import {AfterViewChecked, Component, OnInit} from '@angular/core';
import { Store } from '@ngrx/store';
import { setLanguage } from 'src/app/core/actions/language.actions';
import {ThemeService} from '../../../theme/theme.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewChecked {
  public language = 'en';
  public isLogged = false;
  public isDarkTheme = true;

  constructor(private themeService: ThemeService, private store: Store) { }

  ngOnInit() {
  }

  ngAfterViewChecked(): void {
    this.isDarkTheme = !!localStorage.getItem('selectedTheme') ? localStorage.getItem('selectedTheme') !== 'light' : true;
  }

  changeLanguage() {
    this.language = this.language === 'en' ? 'es' : 'en';
    this.store.dispatch( setLanguage({language: this.language}) );
  }

  toggle() {
    const active = this.themeService.getActiveTheme() ;
    if (active.name === 'light') {
      localStorage.setItem('selectedTheme', 'dark');
      this.themeService.setTheme('dark');
    } else {
      localStorage.setItem('selectedTheme', 'light');
      this.themeService.setTheme('light');
    }
  }
}
