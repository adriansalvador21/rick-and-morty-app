import { createReducer, on } from '@ngrx/store';
import { setEpisodes  } from '../actions/episodes.actions';

export const initialState = {
  episodesList: []
};

export const episodesReducer = createReducer(
  initialState,
  on(setEpisodes, (state, { episodes }) => {
    return {
      ...state,
      episodesList: episodes,
    }
  })
);
