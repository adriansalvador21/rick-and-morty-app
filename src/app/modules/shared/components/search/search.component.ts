import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
})
export class SearchComponent implements OnInit {
  @Output() searchEmitter = new EventEmitter <any>();
  searchValue: any;
  
  constructor() { }

  ngOnInit() {
  }

  searchData() {
    this.searchEmitter.emit(this.searchValue);
  }
}
