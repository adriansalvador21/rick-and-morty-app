import { NgModule } from "@angular/core";
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from "../shared/shared.module";
import { LocationsRoutingModule } from "./locations-routing.module";
import { LocationsListComponent } from "./pages/locations-list/locations-list.component";

@NgModule({
  declarations: [
    LocationsListComponent
  ],
  imports: [
    SharedModule,
    TranslateModule,
    LocationsRoutingModule
  ],
  exports: [],
  providers: [],
  entryComponents: [
  ]
})
export class LocationsModule { }
