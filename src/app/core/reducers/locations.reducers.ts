import { createReducer, on } from '@ngrx/store';
import { setLocations } from '../actions/locations.actions';

export const initialState = {
  locationsList: []
};

export const locationsReducer = createReducer(
  initialState,
  on(setLocations, (state, { locations }) => {
    return {
      ...state,
      locationsList: locations,
    }
  })
);
