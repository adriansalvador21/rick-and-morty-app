import { HttpClient } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { TranslateLoader, TranslateModule, TranslateService, TranslateStore } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { HttpLoaderFactory } from 'src/app/app.module';
import { ActivateRouteMock } from 'src/app/core/models/test-data/ActivateRouteMock';
import { StoreServiceMock } from 'src/app/core/models/test-data/StoreServiceMock';
import { ThemeServiceMock } from 'src/app/core/models/test-data/ThemeServiceMock';
import TranslateMockPipe from 'src/app/core/models/test-data/TranslatePipeMock';
import { TranslateServiceMock } from 'src/app/core/models/test-data/TranslateServiceMock';
import { ThemeService } from 'src/app/modules/theme/theme.service';
import { SharedModule } from '../../shared.module';

import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  const routerSpy = {
    navigate: jasmine.createSpy('navigate'),
    url: '/solicitud/credito-hipotecario/informacion-laboral'
  };

  let translations: any = { "CARDS_TITLE": "This is a test" };

  class FakeLoader implements TranslateLoader {
    getTranslation(lang: string): Observable<any> {
      return of(translations);
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent, TranslateMockPipe],
      providers: [
        { provide: Store, useValue: new StoreServiceMock() },
        { provide: ActivatedRoute, useValue: new ActivateRouteMock() },
        { provide: ThemeService, useValue: new ThemeServiceMock() },
        TranslateService,
        TranslateStore,
        TranslateLoader,
        /*  { provide: TranslateService, useValue: TranslateServiceMock }, */
      ],
      imports: [
        SharedModule,
        RouterTestingModule,
        ReactiveFormsModule,
        TranslateModule.forRoot({
          loader: {provide: TranslateLoader, useClass: FakeLoader},
        })
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should change language', () => {
    component.changeLanguage();
    expect(component.language).toBe('es');
  });
});
