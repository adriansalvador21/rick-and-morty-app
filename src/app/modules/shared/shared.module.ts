import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, RouterModule } from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatExpansionModule} from '@angular/material/expansion';
import { HeaderComponent } from './components/header/header.component';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { SearchComponent } from './components/search/search.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [HeaderComponent, PaginatorComponent, SearchComponent],
  imports: [
    CommonModule,
    TranslateModule,
    RouterModule,
    MatMenuModule,
    MatIconModule,
    MatInputModule,
    MatButtonModule,
    MatExpansionModule,
    FormsModule
  ],
  exports: [
    HeaderComponent,
    MatMenuModule,
    TranslateModule,
    MatIconModule,
    CommonModule,
    MatButtonModule,
    MatExpansionModule,
    FormsModule,
    PaginatorComponent,
    SearchComponent
  ],
  providers: [],
})
export class SharedModule {}
