import { NgModule } from "@angular/core";
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from "../shared/shared.module";
import { EpisodesRoutingModule } from "./episodes-routing.module";
import { EpisodesListComponent } from "./pages/episodes-list/episodes-list.component";

@NgModule({
  declarations: [
    EpisodesListComponent
  ],
  imports: [
    SharedModule,
    TranslateModule,
    EpisodesRoutingModule
  ],
  exports: [],
  providers: [],
  entryComponents: [
  ]
})
export class EpisodesModule { }
