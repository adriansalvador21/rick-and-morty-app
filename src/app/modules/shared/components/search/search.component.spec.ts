import { HttpClient } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { TranslateLoader, TranslateModule, TranslatePipe, TranslateService, TranslateStore } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import TranslateMockPipe from 'src/app/core/models/test-data/TranslatePipeMock';
import { SharedModule } from '../../shared.module';

import { SearchComponent } from './search.component';

describe('SearchComponent test', () => {
    let component: SearchComponent;
    let fixture: ComponentFixture<SearchComponent>;

    const routerSpy = {
        navigate: jasmine.createSpy('navigate'),
        url: '/solicitud/credito-hipotecario/informacion-laboral'
    };

    let translations: any = { "CARDS_TITLE": "This is a test" };

    class FakeLoader implements TranslateLoader {
        getTranslation(lang: string): Observable<any> {
            return of(translations);
        }
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [SearchComponent, TranslateMockPipe],
            providers: [
                TranslateService,
                TranslateStore,
                TranslateLoader,
                /*  { provide: TranslateService, useValue: TranslateServiceMock }, */
            ],
            imports: [
                SharedModule,
                RouterTestingModule,
                ReactiveFormsModule,
                BrowserAnimationsModule,
                TranslateModule.forRoot({
                    loader: { provide: TranslateLoader, useClass: FakeLoader },
                })
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(SearchComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
