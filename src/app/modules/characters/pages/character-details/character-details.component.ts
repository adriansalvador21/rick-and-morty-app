import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { CharacterType } from 'src/app/core/models/types';
import { RickAndMortyService } from 'src/app/core/providers/rick-and-morty.service';

@Component({
  selector: 'app-character-details-page',
  templateUrl: './character-details.component.html',
  styleUrls: ['./character-details.component.scss']
})
export class CharactersDetailsComponent implements OnInit {
  public charactersList: CharacterType[] = [];
  public selectedCharacter: any;

  constructor(public router: Router, private location: Location, public rickAndMortyService: RickAndMortyService, private store: Store<any>, public route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.store.select('appReducer').subscribe(state => {
        if (!this.selectedCharacter && Object.keys(state.characters.selectedCharacter).length) {
          this.selectedCharacter = state.characters.selectedCharacter;
        } else {
          this.getCharacter(params['id'])
        }
      });
    });
  }

  getCharacter(id: any) {
    this.rickAndMortyService.getCharacter(id).subscribe((character) => {
      this.selectedCharacter = {...character}
    });
  }

  backCharacters() {
    this.location.back()
  }
}
