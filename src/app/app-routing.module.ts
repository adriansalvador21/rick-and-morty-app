import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    data: { title: 'Rick&Morty | Home' },
    loadChildren: () => import('../app/modules/public/public.module').then((m) => m.PublicModule)
  },
  {
    path: 'characters',
    data: { title: 'Rick&Morty | Characters' },
    loadChildren: () => import('../app/modules/characters/characters.module').then((m) => m.CharactersModule)
  },
  {
    path: 'locations',
    data: { title: 'Rick&Morty | Locations' },
    loadChildren: () => import('../app/modules/locations/locations.module').then((m) => m.LocationsModule)
  },
  {
    path: 'episodes',
    data: { title: 'Rick&Morty | Episodes' },
    loadChildren: () => import('../app/modules/episodes/episodes.module').then((m) => m.EpisodesModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
