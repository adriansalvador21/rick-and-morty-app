import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateLoader } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { ThemeServiceMock } from 'src/app/core/models/test-data/ThemeServiceMock';
import { ThemeService } from 'src/app/modules/theme/theme.service';
import { SharedModule } from '../../shared.module';

import { PaginatorComponent } from './paginator.component';

describe('PaginatorComponent test', () => {
    let component: PaginatorComponent;
    let fixture: ComponentFixture<PaginatorComponent>;

    const routerSpy = {
        navigate: jasmine.createSpy('navigate'),
        url: '/solicitud/credito-hipotecario/informacion-laboral'
    };

    let translations: any = { "CARDS_TITLE": "This is a test" };

    class FakeLoader implements TranslateLoader {
        getTranslation(lang: string): Observable<any> {
            return of(translations);
        }
    }

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [PaginatorComponent],
            providers: [
                { provide: ThemeService, useValue: new ThemeServiceMock() },
                /*  { provide: TranslateService, useValue: TranslateServiceMock }, */
            ],
            imports: [
                SharedModule,
                RouterTestingModule,
                ReactiveFormsModule,
            ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(PaginatorComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should return a valid array with total pages', () => {
        component.totalPages = 10;
        expect(component.setPages()).not.toBeUndefined();
    });

    it('should generate valid ranges', () => {
        component.pagesNumber = 50;
        component.pageSize = 10;
        component.length = 50;
        component.getRanges();
        expect(component.ranges.length).not.toBe(0);
    });

    it('should retun valid range', () => {
        component.pagesNumber = 50;
        component.pageSize = 10;
        component.length = 50;
        component.getRanges();
        expect(component.getRange(1)).not.toBeUndefined();
    });
});
