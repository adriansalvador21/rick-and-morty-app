import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss'],
})
export class PaginatorComponent implements OnInit {
  @Input() totalPages = 0; 
  @Input() selectedPage = 1;
  @Input() paginatorData: any;
  @Input() pageIndex = 0;
  @Output() pageSelectedEmitter = new EventEmitter<any>();
  public pageSize = 10;
  public length = 0;
  public pagesNumber: any = 0;
  actuallyRange: number = 0;
  ranges: any = [];

  constructor() {}

  async ngOnInit() {
    this.totalPages = this.paginatorData.pages;
    this.length = this.paginatorData.count;
    this.getRanges();
  }
  
  setPages() {
    return new Array(this.totalPages);
  }

  setPrevious() {
    if (this.pageIndex === 0) {
      return;
    }

    this.pageIndex = this.pageIndex - 1;
    this.emitPage();
  }

  setNext() {
    if (this.pageIndex === this.totalPages - 1) {
      return;
    }

    this.pageIndex = this.pageIndex + 1;
    this.emitPage();
  }

  setPage(page: any) {
    this.selectedPage = page;
    this.emitPage();
  }

  emitPage() {
    this.pageSelectedEmitter.emit(this.pageIndex);
  }

  getRanges() {
    let rango = [];
    this.ranges = [];
    this.pagesNumber = Math.ceil(this.length / this.pageSize);
    for (let i = 1; i <= this.pagesNumber; i++) {
      rango.push(i);
      if (rango.length === 5 || i === this.pagesNumber) {
        this.ranges.push(rango);
        rango = [];
      }
    }
  }

  getRange(pageNumber: number) {
    for (let i = 0; i < this.ranges.length; i++) {
      for (let j = 0; j < this.ranges[i].length; j++) {
        if (pageNumber === this.ranges[i][j]) {
          return this.ranges[i];
        }
      }
    }
  }

  changePageInRange(page: number) {
    this.pageIndex = page - 1;
    this.emitPage();
  }
  
}
