import { createAction, props } from '@ngrx/store';

export const setEpisodes = createAction(
    '[Episodes] Set episodes',
    props<{episodes: any}>()
);
