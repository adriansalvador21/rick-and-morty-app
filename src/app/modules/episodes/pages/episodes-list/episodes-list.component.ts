import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { setSelectedCharacter } from 'src/app/core/actions/characters.actions';
import { setEpisodes } from 'src/app/core/actions/episodes.actions';
import { CharacterType, EpisodeType } from 'src/app/core/models/types';
import { RickAndMortyService } from 'src/app/core/providers/rick-and-morty.service';

@Component({
  selector: 'app-episodes-list-page',
  templateUrl: './episodes-list.component.html',
  styleUrls: ['./episodes-list.component.scss']
})
export class EpisodesListComponent implements OnInit {
  public selectedPage = 1;
  public episodesList: EpisodeType[] = [];
  public searchData = '';
  public episodesInfo = null;
  public activePaginator = false;
  public episodesCharacters: any = {};

  constructor(public router: Router, public rickAndMortyService: RickAndMortyService, private store: Store<any>, public route: ActivatedRoute) {
  }

  ngOnInit() {
    this.getEpisodes(0);
  }

  getEpisodes(page: any) {
    this.rickAndMortyService.getEpisodes(page, this.searchData).subscribe((episodes) => {
      this.episodesList = [...episodes.results];
      this.episodesInfo = {...episodes.info};
      this.activePaginator = true;
      this.store.dispatch(setEpisodes({episodes: this.episodesList}));
    });
  }

  setPageIndex(page: any) {
    this.selectedPage = page + 1;
    this.getEpisodes(this.selectedPage);
  }

  getEpisodesCharacters(episode: any) {
    if (!!this.episodesCharacters[episode.id]) {{
      return;
    }}

    let userList = '';
    for (let i = 0; i < episode.characters.length; i++) {
      const startSearch = episode.characters[i].lastIndexOf('/') + 1;
      userList = userList + `${episode.characters[i].substring(startSearch)},`;
    }

    this.getCharacters(userList).subscribe((characters) => {
      this.episodesCharacters[episode.id] = characters;
    });
  }

  getCharacters(group: string) {
    return this.rickAndMortyService.getCharactersGroup(group);
  }

  goToCharacterDetails(character: CharacterType) {
    this.store.dispatch(setSelectedCharacter({character: character}));
    this.router.navigate(['/characters/details', character.id])
  }

  getSearchData(value: any) {
    this.activePaginator = false;
    this.searchData = !!value ? value : '';
    this.selectedPage = 1;
    this.getEpisodes(this.selectedPage);
  }
}
