import { RouterModule, Routes } from '@angular/router';
import { CharactersDetailsComponent } from './pages/character-details/character-details.component';
import { CharactersListComponent } from './pages/characters-list/characters-list.component';

const routes: Routes = [
  {
    path: '',
    component: CharactersListComponent,
    data: {
      title: 'Rick&Morty | Characters',
      description: 'Rick and Morty characters'
    },
  },
  {
    path: 'details/:id',
    component: CharactersDetailsComponent,
    data: {
      title: 'Rick&Morty | Characters details',
      description: 'Rick and Morty characters details'
    },
  },
  { path: '**', redirectTo: '' }
];

export const CharactersRoutingModule = RouterModule.forChild(routes);
