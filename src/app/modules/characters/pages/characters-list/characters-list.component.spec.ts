import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { TranslateLoader, TranslateModule, TranslateService, TranslateStore } from '@ngx-translate/core';
import { Observable, of } from 'rxjs';
import { StoreServiceMock } from 'src/app/core/models/test-data/StoreServiceMock';
import TranslateMockPipe from 'src/app/core/models/test-data/TranslatePipeMock';
import { SharedModule } from 'src/app/modules/shared/shared.module';
import { CharactersListComponent } from './characters-list.component';


describe('CharacterListComponent test', () => {
  let component: CharactersListComponent;
  let fixture: ComponentFixture<CharactersListComponent>;

  const routerSpy = {
    navigate: jasmine.createSpy('navigate'),
    url: '/solicitud/credito-hipotecario/informacion-laboral'
  };

  let translations: any = { "CARDS_TITLE": "This is a test" };

  class FakeLoader implements TranslateLoader {
    getTranslation(lang: string): Observable<any> {
      return of(translations);
    }
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CharactersListComponent, TranslateMockPipe],
      providers: [
        TranslateService,
        TranslateStore,
        TranslateLoader,
        { provide: Store, useValue: StoreServiceMock },
      ],
      imports: [
        SharedModule,
        RouterTestingModule,
        ReactiveFormsModule,
        HttpClientModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot({
          loader: { provide: TranslateLoader, useClass: FakeLoader },
        })
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CharactersListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
