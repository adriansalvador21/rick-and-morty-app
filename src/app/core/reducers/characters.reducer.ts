import { createReducer, on } from '@ngrx/store';
import { setSelectedCharacter, setCharacters } from '../actions/characters.actions';

export const initialState = {
  charactersList: [],
  selectedCharacter: {}
};

export const charactersReducer = createReducer(
  initialState,
  on(setCharacters, (state, { characters }) => {
    return {
      ...state,
      charactersList: characters,
    }
  }),
  on(setSelectedCharacter, (state, { character }) => {
    return {
      ...state,
      selectedCharacter: { ...character },
    }
  }),
);
