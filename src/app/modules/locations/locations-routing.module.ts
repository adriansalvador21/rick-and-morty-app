import { RouterModule, Routes } from '@angular/router';
import { LocationsListComponent } from './pages/locations-list/locations-list.component';

const routes: Routes = [
  {
    path: '',
    component: LocationsListComponent,
    data: {
      title: 'Rick&Morty | Locations',
      description: 'Rick and Morty locations'
    },
  },
  { path: '**', redirectTo: '' }
];

export const LocationsRoutingModule = RouterModule.forChild(routes);
