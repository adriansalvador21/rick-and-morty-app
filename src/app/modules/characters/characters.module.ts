import { NgModule } from "@angular/core";
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from "@angular/common";
import { SharedModule } from "../shared/shared.module";
import { CharactersListComponent } from "./pages/characters-list/characters-list.component";
import { CharactersRoutingModule } from "./characters-routing.module";
import { CharactersDetailsComponent } from "./pages/character-details/character-details.component";

@NgModule({
  declarations: [
      CharactersListComponent,
      CharactersDetailsComponent
  ],
  imports: [
    SharedModule,
    TranslateModule,
    CharactersRoutingModule,
  ],
  exports: [],
  providers: [],
  entryComponents: [
  ]
})
export class CharactersModule { }
