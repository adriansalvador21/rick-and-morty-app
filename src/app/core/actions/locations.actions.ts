import { createAction, props } from '@ngrx/store';

export const setLocations = createAction(
    '[Locations] Set locations',
    props<{locations: any}>()
);
