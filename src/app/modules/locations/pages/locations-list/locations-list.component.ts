import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { setSelectedCharacter } from 'src/app/core/actions/characters.actions';
import { setLocations } from 'src/app/core/actions/locations.actions';
import { CharacterType, LocationType } from 'src/app/core/models/types';
import { RickAndMortyService } from 'src/app/core/providers/rick-and-morty.service';

@Component({
  selector: 'app-locations-list-page',
  templateUrl: './locations-list.component.html',
  styleUrls: ['./locations-list.component.scss']
})
export class LocationsListComponent implements OnInit {
  public selectedPage = 1;
  public locationsList: LocationType[] = [];
  public searchData = '';
  public locationsInfo = null;
  public activePaginator = false;
  public locationCharacters: any = {};

  constructor(public router: Router, public rickAndMortyService: RickAndMortyService, private store: Store<any>, public route: ActivatedRoute) {
  }

  ngOnInit() {
    this.getLocations(0);
  }

  getLocations(page: any) {
    this.rickAndMortyService.getLocations(page, this.searchData).subscribe((locations) => {
      this.locationsList = [...locations.results];
      this.locationsInfo = {...locations.info};
      this.activePaginator = true;
      this.store.dispatch(setLocations({locations: this.locationsList}));
    });
  }

  setPageIndex(page: any) {
    this.selectedPage = page + 1;
    this.getLocations(this.selectedPage);
  }

  getLocationCharacters(location: any) {
    if (!!this.locationCharacters[location.id]) {{
      return;
    }}

    let userList = '';
    for (let i = 0; i < location.residents.length; i++) {
      const startSearch = location.residents[i].lastIndexOf('/') + 1;
      userList = userList + `${location.residents[i].substring(startSearch)},`;
    }

    this.getCharacters(userList).subscribe((characters) => {
      this.locationCharacters[location.id] = characters;
    });
  }

  getCharacters(group: string) {
    return this.rickAndMortyService.getCharactersGroup(group);
  }

  goToCharacterDetails(character: CharacterType) {
    this.store.dispatch(setSelectedCharacter({character: character}));
    this.router.navigate(['/characters/details', character.id])
  }

  getSearchData(value: any) {
    this.activePaginator = false;
    this.searchData = !!value ? value : '';
    this.selectedPage = 1;
    this.getLocations(this.selectedPage);
  }
}
