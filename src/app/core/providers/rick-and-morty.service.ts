import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError, delay } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable({ providedIn: 'root' })
export class RickAndMortyService {
  public apiUrl = environment.apiUrl;

  constructor(private http: HttpClient, private spinner: NgxSpinnerService, public router: Router) {
  }

  getQuery(query: string) {
    this.spinner.show();
    const url = `${this.apiUrl}${query}`;
    const headers = new HttpHeaders({
    });
    return this.http.get(url, { headers });
  }

  getCharactersGroup(group: string) {
    return this.getQuery(`/character/${group}`)
      .pipe(
        delay(300),
        map(data => {
          this.spinner.hide();
          return data as any;
        }),
        catchError((e) => {
          this.spinner.hide();
          return throwError(false);
        })
      );
  }

  getCharactersByPage(page = 0, name = '') {
    return this.getQuery(`/character/?page=${page}&name=${name}`)
      .pipe(
        delay(300),
        map(data => {
          this.spinner.hide();
          return data as any;
        }),
        catchError((e) => {
          this.spinner.hide();
          return throwError(false);
        })
      );
  }

  getCharacter(id: any) {
    return this.getQuery(`/character/${id}`)
      .pipe(
        map(data => {
          this.spinner.hide();
          return data as any;
        }),
        catchError((e) => {
          this.spinner.hide();
          return throwError(false);
        })
      );
  }

  getLocations(page = 0, name = '') {
    return this.getQuery(`/location/?page=${page}&name=${name}`)
      .pipe(
        delay(300),
        map(data => {
          this.spinner.hide();
          return data as any;
        }),
        catchError((e) => {
          this.spinner.hide();
          return throwError(false);
        })
      );
  }

  getEpisodes(page = 0, name = '') {
    return this.getQuery(`/episode/?page=${page}&name=${name}`)
      .pipe(
        delay(300),
        map(data => {
          this.spinner.hide();
          return data as any;
        }),
        catchError((e) => {
          this.spinner.hide();
          return throwError(false);
        })
      );
  }
}
