import { combineReducers } from "@ngrx/store";
import * as fromLanguage from './core/reducers/language.reducer';
import * as fromCharacter from './core/reducers/characters.reducer';
import * as fromEpisodes from './core/reducers/episodes.reducer';
import * as fromLocations from './core/reducers/locations.reducers';

export const appReducer = combineReducers({
  language: fromLanguage.languageReducer,
  characters: fromCharacter.charactersReducer,
  episodes: fromEpisodes.episodesReducer,
  locations: fromLocations.locationsReducer,
});
