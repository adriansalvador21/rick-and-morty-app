import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { setCharacters, setSelectedCharacter } from 'src/app/core/actions/characters.actions';
import { CharacterType } from 'src/app/core/models/types';
import { RickAndMortyService } from 'src/app/core/providers/rick-and-morty.service';

@Component({
  selector: 'app-characters-list-page',
  templateUrl: './characters-list.component.html',
  styleUrls: ['./characters-list.component.scss']
})
export class CharactersListComponent implements OnInit {
  public charactersList: CharacterType[] = [];
  public searchData = '';
  public charactersInfo = null;
  public selectedPage = 1;
  public activePaginator = false;

  constructor(public router: Router, public rickAndMortyService: RickAndMortyService, private store: Store) {
  }

  ngOnInit() {
    this.getCharacters(0);
  }

  goToDetails(character: any) {
    this.store.dispatch(setSelectedCharacter({character: character}));
    this.router.navigate(['/characters/details', character.id])
  }

  getCharacters(page: any) {
    this.rickAndMortyService.getCharactersByPage(page, this.searchData).subscribe((characters) => {
      this.charactersList = [...characters.results];
      this.charactersInfo = {...characters.info};
      this.activePaginator = true;
      this.store.dispatch(setCharacters({characters: this.charactersList}));
    });
  }

  setPageIndex(page: any) {
    this.selectedPage = page + 1;
    this.getCharacters(this.selectedPage);
  }

  getSearchData(value: any) {
    this.activePaginator = false;
    this.searchData = !!value ? value : '';
    this.selectedPage = 1;
    this.getCharacters(this.selectedPage);
  }
}
