import { createAction, props } from '@ngrx/store';
import { CharacterType } from '../models/types';

export const setCharacters = createAction(
    '[Characters] Set Charactes',
    props<{characters: any}>()
);

// TODO set model
export const setSelectedCharacter = createAction(
    '[Characters] Set Selected Character',
    props<{character: CharacterType}>()
);
