export interface CharacterType {
  created: string;
  episode: any;
  gender: string;
  id: number;
  image: string;
  location: LocationType;
  name: string;
  origin: any;
  species: string;
  status: string;
  type: string;
  url: string;
}

export interface LocationType {
  name: string;
  url: string;
  created: string;
  dimension: string;
  id: 1;
  residents: any;
  type: string;
}

export interface EpisodeType {
  air_date: string;
  characters: any;
  created: string;
  episode: string;
  id: number;
  name: string;
  url: string;
}