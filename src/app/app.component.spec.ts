import { TestBed } from '@angular/core/testing';
import { MatMenuModule } from '@angular/material/menu';
import { RouterTestingModule } from '@angular/router/testing';
import { Store } from '@ngrx/store';
import { TranslateLoader, TranslateModule, TranslateService, TranslateStore } from '@ngx-translate/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { Observable, of } from 'rxjs';
import { AppComponent } from './app.component';
import { StoreServiceMock } from './core/models/test-data/StoreServiceMock';
import { ThemeServiceMock } from './core/models/test-data/ThemeServiceMock';
import { TranslateServiceMock } from './core/models/test-data/TranslateServiceMock';
import { HeaderComponent } from './modules/shared/components/header/header.component';
import { ThemeService } from './modules/theme/theme.service';

describe('AppComponent', () => {


  let translations: any = { "CARDS_TITLE": "This is a test" };

  class FakeLoader implements TranslateLoader {
    getTranslation(lang: string): Observable<any> {
      return of(translations);
    }
  }

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot({
          loader: {provide: TranslateLoader, useClass: FakeLoader},
        }),
        MatMenuModule,
        NgxSpinnerModule
      ],
      declarations: [
        AppComponent,
        HeaderComponent
      ],
      providers: [
        { provide: TranslateService, useValue: new TranslateServiceMock()},
        { provide: Store, useValue: new StoreServiceMock()},
        { provide: ThemeService, useValue: new ThemeServiceMock() },
        TranslateService,
        TranslateStore,
        TranslateLoader,
      ]
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'rick-and-morty-app'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('rick-and-morty-app');
  });
});
