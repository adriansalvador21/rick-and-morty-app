import { RouterModule, Routes } from '@angular/router';
import { EpisodesListComponent } from './pages/episodes-list/episodes-list.component';

const routes: Routes = [
  {
    path: '',
    component: EpisodesListComponent,
    data: {
      title: 'Rick&Morty | Episodes',
      description: 'Rick and Morty episodes'
    },
  },
  { path: '**', redirectTo: '' }
];

export const EpisodesRoutingModule = RouterModule.forChild(routes);
