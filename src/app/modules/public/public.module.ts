import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Router, RouterModule } from '@angular/router';
import { HomePageComponent } from './pages/home-page/home-page.component';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [HomePageComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        component: HomePageComponent,
        data: {
          title: 'Rick&Morty | Home',
          description: 'Rick and Morty landing'
        },
      },
    ]),
    TranslateModule,
  ],
  exports: [],
  providers: [],
})
export class PublicModule {}
